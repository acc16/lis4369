import functions as f

def main():
    f.get_requirements()
    f.calc_payroll()

if __name__ == "__main__":
    main()
print("\nSquare Feet to Acres\n")

#Requirements
print("Program Requirements:")
print("1. Find number of IT/ICT students in class.")
print("1. Calculate IT/ICT Student Percentage.")
print("2. Must use float data type (to facilitate right-alignment).")
print("3. Format, right-align numbers, and round conversion to two decimal places.")


print("\nInput:")
it_stu = 0
ict_stu = 0

#calculation
it_stu = float(input("Enter number of IT students: "))
ict_stu = float(input("Enter number of ICT students: "))
total = (it_stu + ict_stu)
print("\nOutput:")

#results
print("{0:17} {1:>5.2f}".format("Total Student:" , total))
print("{0:17} {1:>5.2%}".format("IT Students:" , it_stu/total))
print("{0:17} {1:>5.2%}".format("ICT Students:" , ict_stu/total))
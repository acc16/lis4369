import pandas as pd #Pandas=Python Data Analysis Library
import datetime
import pandas_datareader as pdr #remote data access for pandas
import matplotlib.pyplot as plt
from matplotlib import style

def get_requirements():
    print("Data Analysis 1")

    #Requirements
    print("\nProgram Requirements:")
    print("1. Run demo.py")
    print("2. If errors occur more than likely missing installations.")
    print("3. Test Python Package Installer: pip freeze")
    print("4. Research how to do the following installations: \n\ta. pandas (only if missing).\n\tb. pandas-datareader (only if missing).\n\tc. matplotlib (only if missing).")
    print("5. Create at least three functions that are called by the program: \n\ta. main(): calls at least two other functions.\n\tb.get_requirements(): displays the program requirements.\n\tc. data_anaylsis_1(): displays the following data.")

#read data into pandas dataframe
def data_analysis_1():
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2018, 10, 15)
    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
#statement goes here...
    total_rows = df.count
    print(total_rows)


    print("\nPrint columns:")
    print(df.columns)

    print("\nPrint data frame:")
    print(df)

    print("\nPrint first five lines:")
#statement goes here
    print(df.head(5))

    print("\nPrint last five lines:")
#statement goes here
    print(df.tail(5))
    print("Print first 2 lines:")
#statement goes here
    print(df.head(2))
    print("\nPrint last 2 lines:")
#statement goes here
    print(df.tail(2))
 #research what these styles do!
    style.use('ggplot')
    df['High'].plot()
    df['Low'].plot()
    df['Open'].plot()
    df['Volume'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()
#Ayanna Chukes
#LIS4369 
#Assignment 3 Painting Estimator
def get_requirements():
    #Display title
    print("Painting Estimator")

    #Requirements
    print("\nProgram Requirements:")
    print("1. Calculate home interior paint cost (w/o primer).")
    print("2. Must use float data type for user input.")
    print("3. Must use SQFT_PER_GALLON constant.")
    print("4. Must use iteration structure (aka \loop\).")
    print("5. Format, right-align numbers, and round to two decimal places.")
    print("6. Create at least five functions that are called by the program: \n\ta. main(): calls at least two other functions: get_requirements() and estimate_painting_cost().\n\tb.get_requirements(): displays the program requirements.\n\tc. estimate_painting_cost(): calculates interior home painting, and calls print functions.\n\td.print_painting_estimate():displays painting costs.\n\te.print_painting_percentage():displays painting costs percentages")

def estimate_painting_cost():
    
    check = "y"
    while check.lower() == "y":

        def print_painting_estimate(total_sqft, sqft_per_gal, paint_gallons, price_per_gal, painting_rate):
        #total_sqft, sqft_per_gal, paint_gallons, price_per_gal, painting_rate
        
            print("\nOutput: ")
            
   
    
            print("{0:20} {1:>9}".format("\nItem","Amount"))
            print("{0:20} {1:>9,.2f}".format("Total Sq Ft:", total_sqft))
            print("{0:20} {1:>9,.2f}".format("Sq Ft per Gallon:", sqft_per_gal))
            print("{0:20} {1:>9,.2f}".format("Number of Gallons:", paint_gallons))
            print("{0:20} ${1:>8,.2f}".format("Paint per Gallon:", price_per_gal))
            print("{0:20} ${1:>8,.2f}".format("Labor per Sq Ft:", painting_rate))
        

        def print_painting_percentage(paint_cost, paint_percent, labor_cost, labor_percent, total_cost):
        #paint_cost,paint_percent,labor_cost,labor_percent,total_cost,total_percent
            print("{0:8} {1:>9} {2:>13}".format("\nCost","Amount","Percentage"))
            print("{0:8} ${1:8,.2f} {2:>13.2%}".format("Paint: ", paint_cost, paint_percent))
            print("{0:8} ${1:8,.2f} {2:>13.2%}".format("Labor: ", labor_cost, labor_percent))
            print("{0:8} ${1:8,.2f} {2:>13.2%}".format("Total: ", total_cost, total_percent))
        
        sqft_per_gal =  350
        total_sqft = 0.0

        #User input
        print("\nInput:")
        total_sqft = float(input("Enter total interior sq ft: "))
        price_per_gal = float(input("Enter price per gallon paint: ")) #$30
        painting_rate = float(input("Enter hourly painting rate per sq ft: ")) #$2

        paint_gallons = total_sqft / sqft_per_gal
        paint_cost = paint_gallons * price_per_gal
        labor_cost = total_sqft * painting_rate
        total_cost = paint_cost + labor_cost
        paint_percent = paint_cost / total_cost
        labor_percent = labor_cost / total_cost
        total_percent = paint_percent + labor_percent

        print_painting_estimate(total_sqft, sqft_per_gal, paint_gallons, price_per_gal, painting_rate)
        print_painting_percentage( paint_cost, paint_percent, labor_cost, labor_percent, total_cost)
    #print_painting_estimate(total_sqft, sqft_per_gal, 
    #                    paint_gallons, price_per_gal, painting_rate)
    #print_painting_percentage(
     #   paint_cost, paint_percent, labor_cost, labor_percent, total_cost)
   
    
        
        
        check = input("Estimate another paint job? (y/n)")
        print()


        print("Thank you for using our Painting Estimator")
        print("Please see our website: http://www.mysite.com")
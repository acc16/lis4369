print("\nSquare Feet to Acres\n")

#Requirements
print("Program Requirements:")
print("1. Convert MPG.")
print("2. Must use float data type for user input and calculation.")
print("3. Format and round conversion to two decimal places.")


print("\nInput:")


#calculation
miles = float(input("Enter miles driven: "))
gas = float(input("Enter gallons of fuel used: "))

print("\nOutput:")

#results
print("%0.2f" % miles + " miles driven and " + "%0.2f" % gas + "gallons used = " + "%0.2f" % round(miles/gas, 2) + "mpg")
> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ayanna Chukes

### Assignment 3 Requirements:


1. Calculate home interior paint cost (w/o primer)
2. Must use float data type for user input
3. Must use SQFT_PER_GALLON constant
4.  Must use iteration structure (aka loop).
5. Format, right-align numbers, and round to two decimal places.
6. Create at least five functions that are called by the program: 
a. main(): calls at least two other functions: get_requirements() and estimate_painting_cost().
b.get_requirements(): displays the program requirements.
c. estimate_painting_cost(): calculates interior home painting, and calls print functions.
d.print_painting_estimate():displays painting costs.
e.print_painting_percentage():displays painting costs percentages"



#### README.md file should include the following items:

1. Course title, your name, assignment requirements  , as per A1;
2. Screenshot of running application

Deliverables:
1. Provide Bitbucket read-only access to lis4369 repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4331 Bitbucket

#### Assignment Screenshots:

*Screenshot of Virtual Studio Code user interface running*:

![VSC Screenshot](img/vs1.png)
![VSC Screenshot](img/vs2.png)

*Screenshot of IDLE user interface running*:

![IDLE Screenshot](img/idle1.png)
![IDLE Screenshot](img/idle2.png)


#### Tutorial Links:

*Repository Link:*
[A3 lis4369 Link](https://bitbucket.org/acc16/lis4369/ "Bitbucket lis4369 Locations")



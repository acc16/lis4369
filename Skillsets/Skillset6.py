
print("\nPython Looping Structures\n")

#Requirements
print("Program Requirements:")
print("1. Print while loop.")
print("2. Print for loops using range() function, and implicit and explicit lists.")
print("3. Use break and continue statements.")
print("4. Replicate display below.")


print("\nInput:")


#calculation
num1 = float(input("Enter num1: "))
num2 = float(input("Enter num2: "))

print("Suitable Operators: +, -, *, /, // (integer division), % (modulo operator), ** (power)")
choice = input("Enter operator: ")

if choice == "+":
    print(num1 + num2)

elif choice == "-":
    print(num1 - num2)

elif choice == "*":
    print(num1 * num2)
    
elif choice == "/":
    if num2 == 0:
        print("Cannot divide by zero!")
    else:
        print(num1 / num2)

elif choice == "//":
    if num2 == 0:
        print("Cannot divide by zero!")
    else:
        print(num1 // num2)

elif choice == "%":
    if num2 == 0:
        print("Cannot divide by zero!")
    else:
        print(num1 % num2)

elif choice == "**":
    print(num1 ** num2)

else:
    print("Incorrect operator")

#Ayanna Chukes
#LIS4369 
#Assignment 1 Tip Calculator

#Display title
print("\nPayroll Calculator\n")

#Requirements
print("Program Requirements:")
print("1. Must use float data type for user input(except, 'Party Number').")
print("2. Overtime rate:1.5 times hourly rate(hours over 40).")
print("3. Must format currency with dollar sign, and two decimal places.")

#User input
print("\nUser Input:")
subtotal = float(input("Cost of meal: "))
taxPercent = float(input("Tax percent: "))
tipPercent = float(input("Tip percent: "))
split = int(input("Party Number: "))

#Calculations
tax = round((subtotal * taxPercent) /100, 2)
amountDue = round(subtotal + tax, 2)
gratuity = round((amountDue * tipPercent) / 100, 2)
total = round(subtotal + tax + gratuity, 2)
splitPay = round(total / split, 2)

#Results
print("\nProgram Output:")
print("Subtotal:\t" + '${:,.2f}'.format(subtotal))
print("Tax:\t\t" + '${:,.2f}'.format(tax))
print("Amount Due:\t" + '${:,.2f}'.format(amountDue))
print("Gratuity:\t" + '${:,.2f}'.format(gratuity))
print("Total:\t\t" + '${:,.2f}'.format(total))
print("Split:\t\t" + '${:,.2f}'.format(splitPay))



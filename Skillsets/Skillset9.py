
print("\nPython Lists\n")

#Requirements
print("Program Requirements:")
print("1. Lists (Python Data Structure): mutable, ordered sequence of elements.")
print("2. Lists are mutable/changeable--that is, can insert, update, delete.")
print("3. Create list - using square brackets [list]: my_list = ['cherries', 'apples', 'bananas', 'oranges']")
print("4. Create a program that mirrors the following IPO (input/process/output) format.")
print("Note: user enters numbr of requested list elements, dynamically rendered below (that is, number of elements can change each run).")

print("\nInput:")


#calculation

my_set = {1, 3.14, 2.0, 'four', 'Five'}
print("Print my_set created only using curly brackets: ")
print(my_set)

print("\nPrint type of my_set")
print(type(my_set))

my_set1 = set([1, 3.14, 2.0, 'four', 'Five'])
print("\nPrint my_set1 created using set() function with list:")
print(my_set1)

print("\nPrint type of my_set")
print(type(my_set1))

my_set2 = set([1, 3.14, 2.0, 'four', 'Five'])
print("\nPrint my_set1 created using set() function with tuple:")
print(my_set2)

print("\nPrint type of my_set")
print(type(my_set2))

print("\nOutput: ")
print("Length of my_set:")
print(len(my_set))

print("Discard 'four':")
my_set.discard('four')
print(my_set)

print("Remove 'Five':")
my_set.remove('Five')
print(my_set)

print("Length of my_set:")
print(len(my_set))

#elem = input("\nPlease enter list element")
#os =int(input("Please enter list *index* position(note: must convert to int): "))

print("\nAdd element to set (4) using add() method:")
my_set.add(4)
print(my_set)


print("Length of my_set:")
print(len(my_set))
# print()

print("Display minimum number:")
print(min(my_set))
#print()

print("Display maximum number:")
print(max(my_set))

# print()
print("Display sum numbers:")
print(sum(my_set))

print("Delete all set elements")
my_set.clear()
print(my_set)

print("Length of my_set")
# print("Print number of elements in my_tuple:" + str(len(my_tuple1)))
print(len(my_set))


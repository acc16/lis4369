
print("\nCalorie Percentage\n")

#Requirements
print("Program Requirements:")
print("1. Find calories per grams of far, carbs, and protein.")
print("1. Calculate percentages.")
print("2. Must use float data type.")
print("3. Format, right-align numbers, and round to two decimal places.")


print("\nInput:")


#calculation
fat = float(input("Enter total fat grams: "))
carbs = float(input("Enter total carb grams: "))
protein = float(input("Enter total protein grams: "))


fat_cal = fat * 9
carb_cal = carbs * 4
protein_cal = protein * 4

total_cal = fat_cal + carb_cal + protein_cal

per_fat = fat_cal / total_cal
per_carbs = carb_cal / total_cal
per_protein = protein_cal / total_cal

print("\nOutput:")
 

#results
print("{0:<8} {1:>10} {2:>13}".format("Type","Calories","Percentage"))
print("{0:<8} {1:>10,.2f} {2:>13.2%}".format("Fat",fat_cal,per_fat))
print("{0:<8} {1:>10,.2f} {2:>13.2%}".format("Carbs",carb_cal,per_carbs))
print("{0:<8} {1:>10,.2f} {2:>13.2%}".format("Protein",protein_cal,per_protein))
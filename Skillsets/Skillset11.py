#!/usr/bin/env python3
import random
print("\nPython Dictionaries\n")

#Requirements
print("Program Requirements:")
print("1. Get user beginning  and ending integer values and store in two variables.")
print("2. Display 10 pseudo-random numbers between and including above values.")
print("3. Must use integer data types\n")
print("4. Example 1: Using range() and randint() functions.\n")
print("5. Example 2: Using a list with range() and shuffle() functions.\n")

start = 0
end = 0

print("\nInput:")
start = int(input("Enter beginning value:"))
end = int(input("Enter ending value:"))

print("\nOutput:")

print("Example 1: Using range() and randint() functions:")
for count in range(10):
    print(random.randint(start, end), sep=", " , end=" ")
print()

print("\nExample 2: Using a list with range() and shuffle() functions:")
r = list(range(start, end + 1))
random.shuffle(r)
for i in r:
    print(i, sep=", ", end =" ")
print()
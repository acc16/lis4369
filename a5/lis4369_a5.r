a <- 9
a # print value of a
a + 5 # print a + 5

b <- sqrt(a)
b # print vvalue of b

c <- vector(1,2,5.3,6,-2,4) 
print(c)
typeof(c) #print data type
is.list(c) #False 
is.vector(c)#True

d <- c("one", "two", "three") #character vector
d
typeof(d) #data type

e <- c(TRUE,TRUE,TRUE,FALSE,TRUE,FALSE) #Logical vector
e
typeof(e)

d[1]
my_str <- "Hello World!"
my_str
typeof(my_str)

sqrt(a)
sqrt(c)

a^2 #scalar squared
c^2 #vector squared

min(c)
max(c)
mean(c)
sum(c)

url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv"
mtcars <- read.csv(file=url,head=TRUE,sep=",") # Reads file and assigns variable
cat("*** Report/ Command Requirements ***","\n")
cat("1) Display all data from file:","\n")
mtcars

cat("\n", "2) Display 1st 10 records", "\n"
head(mtcars, 10)

cat("\n", "3) Display last 10 records", "\n"
tail(mtcars, 10)

cat("\n", "4) Display file structure", "\n"
str(mtcars)

cat("\n", "5) Display column names", "\n"
names(mtcars)

cat("\n", "6) Display 1st record/row with column names", "\n"
mtcars[1,]

cat("\n", "7) Display 2nd column data (mpg), using column number:", "\n",
"Note: does not include column name", "\n")
mtcars[,2]

cat("\n", "8) Display column data (cyl), using column name:","\n",
"Notes: return does not display column name", "\n")
mtcars$cyl

cat("\n", "9) Display row/column data (3,4), that is, one field, using square bracket notation")
mtcars[3,4]

cat("\n", "10) Display all data for cars having greater than four cylinders:", "\n")
mtcars[mtcars$cyl > 4,]

cat("\n", "11) Display all cars having more than 4 cylinders and greater than 5 gears", "\n")
mtcars[mtcars$cyl>4  &mtcars$gear > 5,]

cat("\n", "12) Display all cars having more than 4 cylinders *and* exactly 4 gears:", "\n"
mtcars[mtcars$cyl > 4 &mtcars$gear=4,]

cat("\n", "13) Display all cars having more than 4 cylinders or exactly 4 gears:", "\n"
mtcars[mtcars$cyl > 4 |mtcars$gear=4,]

cat("\n", "14) Display all cars having more than 4 cylinders that do not have 4 gears:", "\n"
mtcars[mtcars$cyl > 4 &mtcars$gear = 4,]

cat("\n", "15) Display total number of rows:", "\n"
nrow(mtcars)

cat("\n", "16) Display total number of columns:", "\n"
ncol(mtcars)

cat("\n", "17) Display total number of dimensions (i.e., rows and columns):", "\n"
dim(mtcars)

cat("\n", "18) Display data frame structure - same as info in Python:'data.frame':", "\n"
str(mtcars)

cat("\n", "19) Get mean, median, minimum, maximum, quantiles, variance, and standard deviation of horsepower:", "\n"
cat("\t", "a. Mean: ")
mean(mtcars$hp, na.rm=TRUE)
cat("\t", "b. Median: ")
median(mtcars$hp, na.rm=TRUE)
cat("\t", "c. Min: ")
min(mtcars$hp, na.rm=TRUE)
cat("\t", "d. Max: ")
max(mtcars$hp, na.rm=TRUE)
cat("\t", "e. Quantile: ")
quantile(mtcars$hp, na.rm=TRUE)
cat("\t", "f. Variance: ")
var(mtcars$hp, na.rm=TRUE)
cat("\t", "g. Standard Deviation: ")
sd(mtcars$hp, na.rm=TRUE)

cat("\n", "20) summary() function prints min, max, mean, median, and quantiles (also, number of NA's, if any.):", "\n"
summary(mtcars$hp, na.rm=TRUE)

cat("\n", "20) Two plots (*must* include *your* name in title): 1) Use qplot(); 2) Use plot():", "\n"
library(ggplot2)
qplot(disp, mpg, data=mtcars,
    xlab="Displacement",
    ylab="MPG",
    colour = cyl,
    main= "Ayanna Chukes: Displacement vs. MPG",)

plot(mtcars$mt,mtcars$mpg,
    main="Ayanna Chukes: Weight vs MPG",
    xlab="Weight in Thousands",
    ylab="MPG")
    las=1

sink.reset <- function(){
    for(i in seq_len(sink.number())){
        sink(NULL)
    }
}
sink.reset()
dir()
getwd()
data(mtcars)
names(mtcars)
mtcars$mpg
mtcars$disp
attributes(mtcars)

ls()
mean(titanic$Age)
mean(titanic$Age, )

quantile(titanic$Age, na.rm=TRUE)
min(titanic$Age, na.rm=TRUE)



summary(titanic$Age, na.rm=TRUE)
titanic[!complete.cases(titanic),]

titanic_no_missing_data <- na.omit(titanic)
titanic_no_missing_data

help(stripchart)
stripchart(titanic_no_missing_data$Age)

# histogram
hist(titanic_no_missing_data$Age,main="Distribution of Titanic Passengers Ages",xlab="Ages")

# boxplot
boxplot(titanic_no_missing_data$Age,
    main='Distribution of Titanic Passengers Ages',
    xlab='Ages',
    horizontal=TRUE)

plot(titanic_no_missing_data$Age,titanic_no_missing_data$Survived,
    main="Relationship Between Ages and Survival",
    xlab="Age",
    ylab="Survived")
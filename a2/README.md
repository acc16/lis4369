> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Ayanna Chukes

### Assignment 2 # Requirements:

*Sub-Heading:*

1. The program should be organized with two modules (main.py and functions.py)
2. Be sure to test your program in both visual studio code and IDLE
3. functions.py should contain (get_requirements(), calculate_payroll(), print_pay())
4. main.py should import functions.py

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshots as per examples below.



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshot of Visual Studio Code user interface running*:

![VS Screenshot](img/vs1.png)

![VS Screenshot](img/vs2.png)

*Screenshot of IDLE user interface running*:

![IDLE Screenshot](img/idle1.png)

![IDLE Screenshot](img/idle2.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/acc16/bitbucketstationlocations/ "Bitbucket Station Locations")

*Repository Link:*
[A2 lis4369 Link](https://bitbucket.org/acc16/lis4369/ "Bitbucket lis4369 Locations")


> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ayanna Chukes

### Assignment # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chp:1,2)
4. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator applicaiton running using IDLE
* Screenshot of running 1_tip_calculator applicaiton running using Visual Studio Code
* Bitbucket repo links
* Git commands w/ short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create a new local repository
2. git status - List the files changed and the ones that still need to be added or commited.
3. git add - Add one or more files to staging 
4. git commit - commit changes or files that have been added
5. git push - send changes to master branch of remote repository
6. git pull - fetch and merge changes on the remote server to the working directory
7. git branch - list all the branches in the repo and tell current branch 

#### Assignment Screenshots:

*Screenshot of a1_tip_calculator applicaiton running using IDLE*:

![IDLE Screenshot](img/image1.png)

*Screenshot of a1_tip_calculator applicaiton running using Visual Studio Code*:

![Visual Studio Screenshot](img/image2.png)

*
#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/acc16/bitbucketstationlocations/ "Bitbucket Station Locations")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket lis4369 Link](https://bitbucket.org/acc16/lis4369/ "Bitbucket lis4369 Locations")



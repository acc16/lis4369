
print("\nPython Looping Structures\n")

#Requirements
print("Program Requirements:")
print("1. Print while loop.")
print("2. Print for loops using range() function, and implicit and explicit lists.")
print("3. Use break and continue statements.")
print("4. Replicate display below.")


print("\nInput:")


#calculation
start = 0
end = 0
my_list = []

print("Input: ")
num = int(input("Enter number of list elements: "))
print()

for i in range(num):
    my_element = input('Please enter list element' + str(i + 1) + ":" )
    my_list.append(my_element)

print("\nOutput: ")
print("Print my_list:")
print(my_list)

elem = input("\nPlease enter list element")
pos =int(input("Please enter list *index* position(note: must convert to int): "))

print("\nInsert element into specific position in my_list:")
my_list.insert(pos,elem)
print(my_list)

print("\nCount number of elements in list:")
print(len(my_list))

print("\nSort elements in list alphabetically:")
my_list.sort()
print(my_list)

print("\nReverse list: ")
my_list.reverse()
print(my_list)

print("\Deletes last element from list:")
my_list.pop()
print(my_list)

print("\nDelete second element from list by *index*")
del my_list[1]
print(my_list)

print("\nDelete element from list by *value*")
my_list.remove('cherries')
print(my_list)
print("\nDelete all elements from list")
my_list.clear()
print(my_list)
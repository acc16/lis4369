> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Ayanna Chukes

### Assignment 4 # Requirements:


 1. Run demo.py
 2. If errors, more than likely missing installations
 3. Test Python Package Installer: pip freeze
 4. Research how to install any missing packages:
 5. Create at least three functions that are called by the program:
 a. main(): calls at least two other functions.displays
 b. get_requirements(): displays the program requirements
 c. data_analysis_2(): displays results as per demo.py.
 6. Display graph as per instructions w/in demo.py

#### README.md file should include the following items:

1. Course title, your name, assignment requirements  , as per A1;
2. Screenshot of running application (IDLE and Visual Studio code)

Deliverables:
1. Provide Bitbucket read-only access to lis4369 repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4369 Bitbucket

#### Assignment Screenshots:

*Screenshot of Visual Studio Code running*:

![Visual Studio code](img/vs1.png)
![Visual Studio code](img/vs2.png)
![Visual Studio code](img/graph1.png)

*Screenshot of IDLE interface running*:

![IDLE Screenshot](img/idle1.png)
![IDLE Screenshot](img/idle2.png)
![IDLE Screenshot](img/graph2.png)

#### Links:

*Bitbucket Link:*
[A4 Bitbucket Link](https://bitbucket.org/acc16/lis4369/ "Extensible Enterprise Solutions")



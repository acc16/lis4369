
print("\nPython Tuples\n")

#Requirements
print("Program Requirements:")
print("1. Tuples (Python data structure): *immutable* (cannot be changed!), ordered sequence of elements.")
print("2. Tuples are immutable/unchangeable--that is, cannot insert, update, delete.\tNote: can reassign or delete and *entire* tuple--but, *not* individual items or slices")
print("3. Create tuple using parentheses(tuple): my_tuple1 = (cherries, apples,bananas, oranges)")
print("4. Create tuple (packing), that is, *without* using parentheses(aka tuple'packing'):my_tuple2 = 1, 2,'three','four")
print("5. Python tuple (unpacking), that is, assign values from tuple to sequence of variables: fruit1, fruit2,fruit3,fruit4 = my_tuple1")
print("6. Create a program that mirrors the following IPO (input/proces/output) format")

print("\nInput: Hard coded -- no user input")


#calculation
my_tuple1 = ("cherries","apples","bananas","oranges")
my_tuple2 = 1, 2, "three", "four"

#print("Input: ")
#num = int(input("Enter number of list elements: "))
#print()

#for i in range(num):
 #   my_element = input('Please enter list element' + str(i + 1) + ":" )
 #   my_list.append(my_element)

print("\nOutput: ")
print("Print my_tuple1:")
print(my_tuple1)
print()

#elem = input("\nPlease enter list element")
#os =int(input("Please enter list *index* position(note: must convert to int): "))

print("Print my_tuple2:")
print(my_tuple2)
print()

fruit1,fruit2,fruit3,fruit4 = my_tuple1
print("Print my_tuple1 unpacking:")
print(fruit1,fruit2,fruit3,fruit4)
print()

print("Print thrid element in my_tuple2:")
print(my_tuple2[2])
print()
print("Print \"slice\" of my_tuple1 (second and third elements):")
print(my_tuple1[1:3])

print()
print("Reassign my_tuple2 using parentheses.")
my_tuple2 =(1,2,3,4)
print("Print my_tuple2:")
print(my_tuple2)

print()
print("Print number of elements in my_tuple:" + str(len(my_tuple1)))
print()
print("Print type of my_tuple1:" + str(type(my_tuple1)))
print()

print("Delete my_tuple1: \nNote: will generate error, if trying to print after it no longer exists" )

del my_tuple1



print("\nPython Selection Structures\n")

#Requirements
print("Program Requirements:")
print("1. Use Python selection structure.")
print("2. Prompt user for two numbers, and a suitable operator.")
print("3. Test fpr correct numeric operator.")
print("4. Replicate display below.")


print("\nInput:")

lck = False

#calculation
while lck == False:
    try:
        num1 = float(input("Enter num1: "))
        break
    except ValueError:
        print("Not a valid entry!")
        continue

while lck == False:
    try:
        num2 = float(input("Enter num2: "))
        break
    except ValueError:
        print("Not a valid entry!")
        continue


print("Suitable Operators: +, -, *, /, // (integer division), % (modulo operator), ** (power)")
choice = input("Enter operator: ")
while lck == False:
    if choice == "+":
        print(num1 + num2)
        break

    elif choice == "-":
        print(num1 - num2)
        break

    elif choice == "*":
        print(num1 * num2)
        break
        
    elif choice == "/":
        if num2 == 0:
            print("Cannot divide by zero!")
            while lck == False:
                try:
                    num2 = float(input("Enter num2: "))
                    break
                except ValueError:
                    continue
        else:
            print(num1 / num2)
            break

    elif choice == "//":
        if num2 == 0:
            print("Cannot divide by zero!")
            while lck == False:
                try:
                    num2 = float(input("Enter num2: "))
                    break
                except ValueError:
                    continue
        else:
            print(num1 // num2)
            break

    elif choice == "%":
        if num2 == 0:
            print("Cannot divide by zero!")
            while lck == False:
                try:
                    num2 = float(input("Enter num2: "))
                    break
                except ValueError:
                    continue
        else:
            print(num1 % num2)
            break

    elif choice == "**":
        print(num1 ** num2)
        break

    else:
        print("Not an operator")
        choice = input("Enter operator: ")
        continue

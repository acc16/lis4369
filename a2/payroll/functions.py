#Ayanna Chukes
#LIS4369 
#Assignment 3 Painting Estimator
def get_requirements():
    #Display title
    print("Painting Estimator")

    #Requirements
    print("Program Requirements:")
    print("1. Calculate home interior paint cost (w/o primer).")
    print("1. Must use float data type for user input.")
    print("4. Must use SQFT_PER_GALLON constant.")
    print("2. Must use iteration structure (aka "loop").")
    print("3. Format, right-align numbers, and round to two decimal places.")
    print("5. Create at least five functions that are called by the program: \n\ta. main(): calls at least two other functions: get_requirements() and estimate_painting_cost().\n\tb.get_requirements(): displays the program requirements.\n\tc. estimate_painting_cost(): calculates interior home painting, and calls print functions.\n\td.print_painting_estimate():displays painting costs.\n\te.print_painting_percentage():displays painting costs percentages")

def estimate_painting_cost():
    
    sqft = 0
    price_per_gal = 0
    hourly_paint_rate = 0
    num_gal = 0
    ovrTime_rate = 1.5
    holi_rate = 2

    lck = False

    #User input
    print("\nInput:")
    while lck == False:
        try:
            sqft = float(input("Enter total interior sq ft: "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an incorrect value. Please enter a decimal number")
            
    lck = False
    while lck == False:
        try: 
            holi_hours = float(input("Enter holiday hours: "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an incorrect value. Please enter a decimal number")

    lck = False
    while lck == False:
        try: 
            rate = float(input("Enter hourly pay rate: "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an incorrect value. Please enter a decimal number")

    #Calculate Overtime
    ovr_time = hours - max_hours

    if ovr_time < 0:
        ovr_time = 0

    hours = hours - ovr_time

    #Calculate Hours
    hours = hours * rate

    #Calculate overtime
    ovr_time = ovr_time * (rate * ovrTime_rate)

    # Calculate Holiday hours
    holi_hours = holi_hours * (rate * holi_rate)

    #Find Total
    total = hours + ovr_time + holi_hours

    #Print Results
    print_pay(hours, holi_hours, ovr_time, total)

def print_pay(hours, holi_hours, ovr_time, pay):
    #Results    
    print("\nOutput:")
    print("Base:      " + '${:,.2f}'.format(round(hours)))
    print("Overtime:  " + '${:,.2f}'.format(round(ovr_time)))
    print("Holiday:   " + '${:,.2f}'.format(round(holi_hours)))
    print("Gross:     " + '${:,.2f}'.format(round(pay)))

# LIS4369

## Ayanna Chukes

### Project 1 # Requirements:

1. Code and run demo.py (packages must be installed).
2. Then use it to backward-engineer the screen shots below it.
3. Installing Python packages (pip).
4. Do own research on Python pacakage installing and updating

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of running application in Visual Code Studio;
3. Screenshot of running application in IDLE;

Deliverables:
1. Provide Bitbucket read-only access to lis43repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4369 Bitbucket

#### Assignment Screenshots:

*Screenshot of VS*:

![Virtual Studio Code Screenshot](img/vs1.png)
![Virtual Studio Code Screenshot](img/vs2.png)

![Virtual Studio Code Screenshot](img/graph1.png)

*Screenshot of IDLE*:

![IDLE Screen Screenshot](img/idle1.png)
![IDLE Screen Screenshot](img/idle2.png)

![IDLE Screen Screenshot](img/graph2.png)
#### Tutorial Links:

*P1 Development:*
[P1 Lis4369 Link](https://bitbucket.org/acc16/lis4369)


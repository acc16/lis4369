print("\nSquare Feet t Acres\n")

#Requirements
print("Program Requirements:")
print("1. Research: number of square feet to acre of land.")
print("2. Must use float data type for user input and calculation.")
print("3. Format and round conversion to two decimal places.")


print("\nInput:")


#calculation
sqfeet = int(input("Enter square feet: "))
total = round(sqfeet * 2.2957e-5, 2)
print("\nOutput:")

#results
print(float(sqfeet) + " = " +  float(total) , " acres")
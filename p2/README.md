> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Ayanna Chukes

### Project 2 # Requirements:


1. Use Assignment 5 screenshots and references above for the      following requirements 
2. Code and run lis4369_p2.R
3. must include at least 2 plots



#### README.md file should include the following items:

1. Course title, your name, assignment requirements  , as per A1;
2. Screenshot of running application;


Deliverables:
1. Provide Bitbucket read-only access to lis4368 repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4369 Bitbucket

#### Assignment Screenshots:

*Screenshot of unpopulated user interface running*:

![Plot 1 Screenshot](img/img1.png)

*Screenshot of populated user interface running*:

![Plot 2 Screenshot](img/img2.png)

*Screenshot of populated user interface running*:

![Plot 3 Screenshot](img/img3.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[P2 Bitbucket Link](https://bitbucket.org/acc16/lis4369/ "Extensible Enterprise Solutions")



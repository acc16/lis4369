
print("\nPython Selection Structures\n")

#Requirements
print("Program Requirements:")
print("1. Use Python selection structure.")
print("2. Prompt user for two numbers, and a suitable operator.")
print("3. Test fpr correct numeric operator.")
print("4. Replicate display below.")


print("\nInput:")


#calculation
num1 = float(input("Enter num1: "))
num2 = float(input("Enter num2: "))

print("Suitable Operators: +, -, *, /, // (integer division), % (modulo operator), ** (power)")
choice = input("Enter operator: ")

if choice == "+":
    print(num1 + num2)

elif choice == "-":
    print(num1 - num2)

elif choice == "*":
    print(num1 * num2)
    
elif choice == "/":
    if num2 == 0:
        print("Cannot divide by zero!")
    else:
        print(num1 / num2)

elif choice == "//":
    if num2 == 0:
        print("Cannot divide by zero!")
    else:
        print(num1 // num2)

elif choice == "%":
    if num2 == 0:
        print("Cannot divide by zero!")
    else:
        print(num1 % num2)

elif choice == "**":
    print(num1 ** num2)

else:
    print("Incorrect operator")

#!/usr/bin/env python3
import math
print("\nPython Dictionaries\n")

#Requirements
print("Program Requirements:")
print("1. Program converts user-entered temperatures into Farenheit.")
print("2. Program continues to prompt for user-entry until no longer requested.")
print("3. Note: upper or lowercase letters permitted. Though, incorrect entries are not permitted")
print("4. Note: Program does not validate numeric data (optional requirement)")


diameter = 0 
volume = 0.0
gallons = 0.0
choice = ''
print("\nInput:")
choice = input("Do you want to calculate a sphere volume(y or n?):").lower()

print("\nOutput:")

while (choice[0] =='y'):
    #diameter = int(input("Please enter diameter in inches:"))
    test = True
    while test == True:
        try:
            diameter = int(input("Please enter diameter in inches:"))
            volume = ((4.0/3.0)*math.pi*math.pow(diameter/2.0,3))
            gallons = volume/231
            print("\nSphere volume:" , str(round(gallons, 2)) , "liquid U.S. gallons\n")
            test = False
        except ValueError:
            print("\nNot valid integer!")
            continue
        choice = input("Do you want to calculate another sphere volume?").lower()
    
print("\nThank you for using our Sphere Calculator!")
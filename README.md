> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4331 Advanced Mobile App Development

## Ayanna Chukes

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/acc16/lis4369/src/master/a1/ "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code 
    - Create a1_tip_calculator application
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/acc16/lis4369/src/master/a2/ "My A2 README.md file")
    - The program should be organized with two modules (main.py and functions.py)
    - Be sure to test your program in both visual studio code and IDLE
    - Functions.py should contain (get_requirements(), calculate_payroll(), print_pay())
    - Main.py should import functions.py

3. [A3 README.md](A3/README.md "My A2 README.md file")
    - 1. Calculate home interior paint cost (w/o primer)
    - 2. Must use float data type for user input
    - 3. Must use SQFT_PER_GALLON constant
    - 4.  Must use iteration structure (aka loop).
    - 5. Format, right-align numbers, and round to two decimal places.
    - 6. Create at least five functions that are called by the program: 
    - a. main(): calls at least two other functions: get_requirements() and estimate_painting_cost().
    - b.get_requirements(): displays the program requirements.
    - c. estimate_painting_cost(): calculates interior home painting, and calls print functions.
    - d.print_painting_estimate():displays painting costs.
    - e.print_painting_percentage():displays painting costs percentages"

4.  [A4 README.md](a4/README.md "My A2 README.md file")
    - 1. Run demo.py
    - 2. If errors, more than likely missing installations
    - 3. Test Python Package Installer: pip freeze
    - 4. Research how to install any missing packages:
    - 5. Create at least three functions that are called by the program:
    - a. main(): calls at least two other functions.displays
    - b. get_requirements(): displays the program requirements
    - c. data_analysis_2(): displays results as per demo.py.
    - 6. Display graph as per instructions w/in demo.py


5.  [A5 README.md](a5/README.md "My A2 README.md file")
    1. Complete R tutorial
    2. Code and run lis4369_a5.R
    3. must include at least 2 plots


6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Code and run demo.py (packages must be installed).
    - Then use it to backward-engineer the screen shots below it.
    - Installing Python packages (pip).
    - Do own research on Python pacakage installing and updating


7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Use Assignment 5 screenshots and references above for the following requirements:
    - Backward-engineer the lis4369_p2_requirements.txt file.
    - Include at least two plots in your README.md file.

